#!/usr/bin/env node

const app = require( "../lib/app" ).default;
const Routes = require( "../lib/routes" ).default;
const DB = require( "../lib/database" );
const http = require( "http" );
const debug = require( "debug" )( "server" );
const { todo } = require( "../package.json" );
// import config from "../src/config";

const normalizePort = ( val ) => {
    const port = parseInt( val, 10 );

    if ( isNaN( port ) ) {
        return val;
    }

    if ( port >= 0 ) {
        return port;
    }

    return false;
};

const onError = ( error ) => {
    if ( error.syscall !== "listen" ) {
        throw error;
    }

    const bind = typeof port === "string"
        ? "Pipe " + port
        : "Port " + port;

    switch ( error.code ) {
        case "EACCES":
            console.error( bind + " requires elevated privileges" );
            process.exit( 1 );
            break;
        case "EADDRINUSE":
            console.error( bind + " is already in use" );
            process.exit( 1 );
            break;
        default:
            throw error;
    }
};

const onListening = () => {
    const addr = server.address();
    const bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr.port;
    debug( "Listening on " + bind );
};

const port = normalizePort( process.env.PORT /*|| config.port*/ || todo.port || "3000" );
app.set( "port", port );

const server = http.createServer( app );
server.listen( port );
server.on( "error", onError );
server.on( "listening", onListening );

DB.MySQL.sequelize.sync( { force: false } ).then( () => {
    console.log( "Database connected." );
} ).catch( error => {
    console.log( "Error: Can't connect to database.", error );
} );
