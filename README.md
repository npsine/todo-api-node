# todo-api-node

## Common setup

Clone the repo and install the dependencies.

```bash
npm install
```

## Start Server

To start the express server, run the following

```bash
npm run start
```

Open [http://localhost:3000](http://localhost:3000) and take a look around.

## Database Config

This project using mySQL please create database name "project-todo" before running

Ex.

dbname: "project-todo",

username: "root",

password: "root",

host: "localhost",

driver: "mysql"
