export default (sequelize, DataTypes) => {
	return sequelize.define("user", {
        fullname: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		email: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		password: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
	});
};
