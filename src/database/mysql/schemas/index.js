export default sequelize => ( {
    Users: sequelize.import( "./user.schema.js" ),
    Forgot: sequelize.import( "./forgot.schema.js" ),
    Todos: sequelize.import( "./todo.schema.js" )
} );
