export default (sequelize, DataTypes) => {
	return sequelize.define("forgot", {
		token: {
			type: DataTypes.STRING,
			defaultValue: ""
		},
		used: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	});
};
