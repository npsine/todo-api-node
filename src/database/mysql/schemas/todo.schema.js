export default ( sequelize, DataTypes ) => {
    return sequelize.define( "todo", {
        todo: {
            type: DataTypes.STRING,
            defaultValue: ""
        },
        userId: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        isFinished: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    } );
};
