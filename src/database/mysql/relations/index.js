import { Schemas } from "../setup";

export default () => {
    Schemas.Users.hasMany( Schemas.Forgot );
    Schemas.Forgot.belongsTo( Schemas.Users );
};
