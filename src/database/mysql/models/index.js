import * as Users from "./user.model";
import * as Forgot from "./forgot.model";
import * as Todo from "./todo.model";

export default {
    Users,
    Forgot,
    Todo
};
