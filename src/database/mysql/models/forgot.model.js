import { Schemas } from "../setup";
import { toPlain } from "../../../util";
import { formatUser } from "./user.model";

export const formatForgot = ( forgot ) => {
    return {
        id: forgot.id,
        token: forgot.token,
        user: forgot.user ? formatUser( forgot.user ) : null,
        available: `${ forgot.used }` === "0"
    };
};

export const createForgot = ( { token, userId } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Forgot.create( {
            token: token,
            userId: userId
        } ).then( forgotCreated => {
            forgotCreated = toPlain( forgotCreated );
            getForgotById( {
                id: forgotCreated.id
            } ).then( forgot => {
                resolve( forgot );
            } ).catch( error => {
                reject( error );
            } );
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const getForgotById = ( { id = 0 } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Forgot.findOne( {
            where: {
                id: id
            },
            include: [ {
                model: Schemas.Users
            } ]
        } ).then( forgot => {
            resolve( formatForgot( toPlain( forgot ) ) );
        } ).catch( error => {
            reject( error );
        } );
    } );
};


export const getForgotByToken = ( { token = 0 } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Forgot.findOne( {
            where: {
                token: token
            },
            include: [ {
                model: Schemas.Users
            } ]
        } ).then( forgot => {
            console.log( "in: ", forgot );
            if ( forgot ) {
                resolve( formatForgot( toPlain( forgot ) ) );
            } else {
                reject();
            }
        } ).catch( error => {
            reject( error );
        } );
    } );
};
