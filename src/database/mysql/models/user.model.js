import { Schemas } from "../setup";
import { toPlain, BCryptAuthenticator } from "../../../util";

const authenticator = new BCryptAuthenticator();

export const formatUser = ( user ) => {
    return {
        ...user
    };
};

export const createUser = ( { fullname = "", email = "", password = "" } ) => {
    return new Promise( ( resolve, reject ) => {
        authenticator.generateHash( password ).then( encryptedPassword => {
            return Schemas.Users.create( {
                email,
                password: encryptedPassword,
                fullname
            } );
        } ).then( userCreated => {
            resolve( formatUser( toPlain( userCreated ) ) );
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const getUserByEmailPassword = ( { email = "", password = "" } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Users.findOne( {
            where: {
                email: email
            }
        } ).then( user => {
            if ( !user ) {
                resolve( null );
                return;
            }
            user = toPlain( user );
            return authenticator.compare( password, user.password );
        } ).then( verified => {
            if ( !verified ) {
                resolve( null );
                return;
            }
            return Schemas.Users.findOne( {
                where: {
                    email: email
                }
            } );
        } ).then( user => {
            if ( user ) {
                resolve( formatUser( toPlain( user ), true ) );
            } else {
                resolve( null );
            }
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const getUserByEmail = ( { email = "" } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Users.findOne( {
            where: {
                email: email
            }
        } ).then( user => {
            if ( user ) {
                resolve( formatUser( toPlain( user ), true ) );
            } else {
                resolve( null );
            }
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const getUserById = ( { id = 0 } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Users.findOne( {
            where: {
                id: id
            }
        } ).then( user => {
            if ( user ) {
                resolve( formatUser( toPlain( user ), true ) );
            } else {
                resolve( null );
            }
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const updateUserPassword = ( { id = 0, password = "" } ) => {
    return new Promise( ( resolve, reject ) => {
        console.log("update password")
        authenticator.generateHash( password ).then( encryptedPassword => {
            return Schemas.Users.update( {
                password: encryptedPassword
            }, {
                where: {
                    id: id
                }
            } );
        } ).then( () => {
            getUserById( {
                id: id
            } ).then( user => {
                resolve( user );
            } ).catch( error => {
                reject( error );
            } );
        } );
    } );
};
