import { Schemas } from "../setup";
import { toPlain } from "../../../util";

export const getTodoList = ( { userId } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Todos.findAll( {
            where: {
                userId: userId,
                deletedAt: null
            },
            order: [ [ "id", "desc" ] ]
        } ).then( todos => {
            if ( todos && todos.length > 0 ) {
                resolve( todos.map( todo => formatTodo( toPlain( todo ) ) ) );
            } else {
                resolve( null );
            }
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const getTodo = ( { id } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Todos.findOne( {
            where: {
                id,
                deletedAt: null
            }
        } ).then( todo => {
            if ( todo ) {
                resolve( formatTodo( toPlain( todo ) ) );
            } else {
                resolve( null );
            }
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const createTodo = ( { todo, userId } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Todos.create( {
            userId: userId,
            todo: todo
        } ).then( todoCreated => {
            todoCreated = toPlain( todoCreated );
            getTodoById( {
                id: todoCreated.id,
                userId
            } ).then( forgot => {
                resolve( forgot );
            } ).catch( error => {
                reject( error );
            } );
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const updateTodo = ( { id, todo, userId } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Todos.update( {
            todo: todo
        }, {
            where: { id }
        } ).then( () => {
            getTodoById( {
                id,
                userId
            } ).then( todo => {
                resolve( todo );
            } ).catch( error => {
                reject( error );
            } );
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const finishTodo = ( { id, isFinished, userId } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Todos.update( {
            isFinished: isFinished
        }, {
            where: { id }
        } ).then( () => {
            getTodoById( {
                id,
                userId
            } ).then( todo => {
                resolve( todo );
            } ).catch( error => {
                reject( error );
            } );
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const getTodoById = ( { id = 0, userId = 0 } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Todos.findOne( {
            where: { id, userId }
        } ).then( todo => {
            if ( todo ) {
                resolve( formatTodo( toPlain( todo ) ) );
            } else {
                resolve( null );
            }
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const deleteTodo = ( { id = 0 } ) => {
    return new Promise( ( resolve, reject ) => {
        Schemas.Todos.destroy( {
            where: { id }
        } ).then( () => {
            resolve();
        } ).catch( error => {
            reject( error );
        } );
    } );
};

export const formatTodo = todo => {
    return {
        ...todo
    };
};
