import express from "express";
import cookieParser from "cookie-parser";
import cors from "cors";
import logger from "morgan";
import compression from "compression";
import rfs from "rotating-file-stream";
import { BUILD_DIR, PUBLIC_DIR, LOG_DIR } from "./paths";
import { ApiRoute } from "./routes";

const app = express();
app.use( logger( "combined", {
    stream: rfs( () => {
        const date = new Date();
        const pad = ( num ) => parseInt( num ) < 10 ? `0${ num }` : `${ num }`;
        return `${ date.getFullYear() }-${ pad( date.getMonth() ) }-${ pad( date.getDate() ) }_access.log`;
    }, {
        interval: "1d",
        path: LOG_DIR
    } )
} ) );

app.use( express.json( { limit: "50mb" } ) );
app.use( express.urlencoded( { limit: "50mb", extended: true } ) );
app.use( cookieParser() );
app.use( cors() );
app.use( compression() );

app.use( express.static( BUILD_DIR ) );
app.use( express.static( PUBLIC_DIR ) );
app.use( "/api", ApiRoute() );

export default app;
