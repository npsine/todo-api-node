import bcrypt from "bcrypt";

export class BCryptAuthenticator {
    constructor() {
        this.saltRounds = 10;
    }

    generateHash( rawPassword ) {
        return new Promise( ( resolve, reject ) => {
            bcrypt.hash( rawPassword, this.saltRounds, ( err, hash ) => {
                if ( err ) {
                    reject( err );
                } else {
                    resolve( hash );
                }
            } );
        } );
    }

    compare( rawPassword, hash ) {
        return new Promise( ( resolve, reject ) => {
            bcrypt.compare( rawPassword, hash, ( err, result ) => {
                if ( err ) {
                    reject( err );
                } else {
                    resolve( result );
                }
            } );
        } );
    }
}
