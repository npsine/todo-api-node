export const toPlain = obj => JSON.parse( JSON.stringify( obj ) );

export const successResponse = ( payload = {} ) => {
    return {
        success: true,
        response: payload
    };
};

export const errorResponse = ( message = "" ) => {
    return {
        success: false,
        error: message
    };
};

export const getUniqueString = ( separate = "_", length = 10 ) => {
    return `${ timestamp() }${ separate }${ randomString( length ) }`;
};

export const timestamp = () => {
    return Math.floor( new Date() / 1000 );
};

export const randomString = ( length = 10 ) => {
    let text = "";
    let possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for ( let i = 0; i < length; i++ ) {
        text += possible.charAt( Math.floor( Math.random() * possible.length ) );
    }

    return text;
};
