import nodemailer from "nodemailer";
import emailTemplate from "./emailTemplate";

export const sendMail = ( {
                              from = "\"TODO WEBAPP ASSIGNMENT\" <nattidap@petpaw.com>",
                              to = "",
                              subject = "",
                              text,
                              html
                          } ) => {
    return new Promise( ( resolve, reject ) => {
        let transporter = nodemailer.createTransport( {
            host: "smtp.gmail.com",
            port: 587,
            secure: false,
            authMethod: "PLAIN",
            service: "gmail",
            auth: {
                user: "nattidap@petpaw.com",
                pass: "efdfoztscesyandm"
            }
        } );

        let mailOptions = {
            from: from,
            to: to,
            subject: subject
        };

        mailOptions = text ? { ...mailOptions, text } : mailOptions;
        mailOptions = html ? { ...mailOptions, html } : mailOptions;

        transporter.sendMail( mailOptions, ( error, info ) => {
            if ( error ) {
                console.log( "Send Mail Error: ", error );
                reject( error );
            } else {
                console.log( "Send Mail Success: ", info );
                resolve();
            }
        } );
    } );
};

export const parseTemplate = ( html = "", btnLink = "" ) => {
    html = html.replace( "{{btnLink}}", btnLink );
    return html;
};

export const sendMailForgot = ( { to = "", btnLink = "" } ) => {
    return new Promise( ( resolve, reject ) => {
        let html = emailTemplate.forgotPassword;
        sendMail( {
            to: to,
            subject: `Forgot password.`,
            html: parseTemplate( html, btnLink )
        } ).then( () => {
            resolve();
        } ).catch( error => {
            reject( error );
        } );
    } );
};
