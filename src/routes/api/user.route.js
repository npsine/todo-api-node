import express from "express";
import { MySQL } from "../../database";
import { generateToken, verifyToken, generateAccessToken, generateRefreshToken } from "../../middlewares/token";
import {
    errorResponse, getUniqueString,
    successResponse
} from "../../util";
import { sendMailForgot } from "../../util/email";

const { Models } = MySQL;

export default () => {
    const router = express.Router();

    router.get( "/", verifyToken, ( req, res ) => {
        const userId = res.locals.authInfo ? res.locals.authInfo.user.id : 0;
        if ( userId ) {
            Models.Users.getUserById( {
                id: res.locals.authInfo.user.id
            } ).then( user => {
                res.json( successResponse( {
                    user: user
                } ) );
            } ).catch( error => {
                res.json( errorResponse( "User not found." ) );
            } );
        } else {
            res.json( errorResponse( "Unauthorized." ) );
        }
    } );

    router.post( "/", verifyToken, ( req, res ) => {
        console.log( req );
        if ( req.body.fullname && req.body.email && req.body.password ) {
            Models.Users.getUserByEmail( {
                email: `${ req.body.email }`.toLowerCase().trim()
            } ).then( user => {
                console.log( user );
                if ( user ) {
                    res.json( errorResponse( "Email already exists.." ) );
                } else {
                    Models.Users.createUser( {
                        fullname: req.body.fullname,
                        email: req.body.email,
                        password: req.body.password
                    } ).then( user => {
                        res.json( successResponse( {
                            user: user
                        } ) );
                    } ).catch( error => {
                        res.json( errorResponse( "Cannot Create User.." ) );
                    } );
                }
            } );
        } else {
            console.log( "Missing require parameter." );
            res.json( errorResponse( "Missing require parameter." ) );
        }
    } );

    router.post( "/login", ( req, res ) => {
        if ( req.body.email && req.body.password ) {
            Models.Users.getUserByEmailPassword( {
                email: `${ req.body.email }`.toLowerCase().trim(),
                password: req.body.password
            } ).then( user => {
                const payload = {
                    user: {
                        id: user.id
                    }
                };
                Promise.all( [ generateAccessToken( payload ), generateRefreshToken( payload ) ] ).then( ( [ accessToken, refreshToken ] ) => {
                    res.cookie( "refresh-token", refreshToken, {
                        maxAge: 60 * 60 * 24 * 7
                    } );

                    res.cookie( "access-token", accessToken );

                    res.json( successResponse( {
                        token: accessToken,
                        user: user
                    } ) );
                } ).catch( error => {
                    console.log( error );
                    res.json( errorResponse( "Internal server error." ) );
                } );
            } ).catch( error => {
                console.log( error );
                res.json( errorResponse( "Unauthorized." ) );
            } );
        } else {
            res.json( errorResponse( "Missing require parameter." ) );
        }
    } );

    router.post( "/forgot", ( req, res ) => {
        if ( req.body.email ) {
            Models.Users.getUserByEmail( {
                email: `${ req.body.email }`.toLowerCase().trim()
            } ).then( user => {
                Models.Forgot.createForgot( {
                    token: `${ getUniqueString( "+", 20 ) }${ user.id }`,
                    userId: user.id
                } ).then( forgot => {
                    sendMailForgot( {
                        to: user.email,
                        btnLink: `http://localhost:4200/reset?token=${ forgot.token }`
                    } ).then( () => {
                        res.json( successResponse( {
                            user: user
                        } ) );
                    } ).catch( error => {
                        res.json( errorResponse( "Send E-mail error." ) );
                    } );
                } ).catch( error => {
                    res.json( errorResponse( "Internal server error." ) );
                } );
            } ).catch( error => {
                res.json( errorResponse( "Not found." ) );
            } );
        } else {
            res.json( errorResponse( "Missing require parameter." ) );
        }
    } );

    router.post( "/reset", ( req, res ) => {
        if ( req.body.token && req.body.password ) {
            console.log(req.body.token)
            Models.Forgot.getForgotByToken( {
                token: `${ req.body.token }`.trim()
            } ).then( forgot => {
                console.log("out: ", forgot)
                Models.Users.updateUserPassword( {
                    id: forgot.user.id,
                    password: req.body.password
                } ).then( user => {
                    res.json( successResponse( {
                        user: user
                    } ) );
                } ).catch( error => {
                    res.json( errorResponse( "Internal server error." ) );
                } );
            } ).catch( error => {
                res.json( errorResponse( "Wrong token." ) );
            } );
        } else {
            res.json( errorResponse( "Missing require parameter." ) );
        }
    } );
    return router;
};
