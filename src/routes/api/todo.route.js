import express from "express";
import { verifyToken } from "../../middlewares/token";
import { errorResponse, successResponse } from "../../util";
import { MySQL } from "../../database";

export default () => {
    const router = express.Router();

    const { Models } = MySQL;

    router.get( "/", verifyToken, ( req, res ) => {
        const userId = res.locals.authInfo ? res.locals.authInfo.user.id : 0;
        Models.Todo.getTodoList( {
            userId: userId
        } ).then( todo => {
            res.json( successResponse( {
                todo: todo
            } ) );
        } ).catch( error => {
            res.json( errorResponse( "Cannot get Todo.." ) );
        } );
    } );

    router.get( "/:id", verifyToken, ( req, res ) => {
        const userId = res.locals.authInfo ? res.locals.authInfo.user.id : 0;
        Models.Todo.getTodo( {
            id: req.params.id
        } ).then( todo => {
            res.json( successResponse( {
                todo: todo
            } ) );
        } ).catch( error => {
            res.json( errorResponse( "Cannot get Todo.." ) );
        } );
    } );

    router.post( "/", verifyToken, ( req, res ) => {
        const userId = res.locals.authInfo ? res.locals.authInfo.user.id : 0;
        if ( req.body.todo ) {
            Models.Todo.createTodo( {
                todo: req.body.todo,
                userId: userId
            } ).then( todo => {
                res.json( successResponse( {
                    todo: todo
                } ) );
            } ).catch( error => {
                res.json( errorResponse( "Cannot Create Todo.." ) );
            } );
        } else {
            res.json( errorResponse( "Missing data." ) );
        }
    } );

    router.post( "/:id/delete", verifyToken, ( req, res ) => {
        const userId = res.locals.authInfo ? res.locals.authInfo.user.id : 0;
        Models.Todo.getTodoById( {
            id: req.params.id,
            userId: userId
        } ).then( todo => {
            console.log( todo );
            if ( `${ todo.userId }` === `${ userId }` ) {
                Models.Todo.deleteTodo( {
                    id: req.params.id
                } ).then( () => {
                    res.json( successResponse( {
                        id: req.params.id
                    } ) );
                } ).catch( error => {
                    console.log( "Error: ", error );
                    res.status( 404 ).json( errorResponse( "Not found." ) );
                } );
            } else {
                res.status( 401 ).json( errorResponse( "Unauthorized." ) );
            }
        } ).catch( error => {
            console.log( "Error: ", error );
            res.status( 500 ).json( errorResponse( "Internal server error." ) );
        } );
    } );

    router.post( "/:id", verifyToken, ( req, res ) => {
        const userId = res.locals.authInfo ? res.locals.authInfo.user.id : 0;
        if ( req.body.todo ) {
            Models.Todo.updateTodo( {
                id: req.params.id,
                todo: req.body.todo,
                userId: userId
            } ).then( todo => {
                res.json( successResponse( {
                    todo: todo
                } ) );
            } ).catch( error => {
                res.json( errorResponse( "Cannot Update Todo.." ) );
            } );
        } else {
            res.json( errorResponse( "Missing data." ) );
        }
    } );

    router.post( "/finish/:id", verifyToken, ( req, res ) => {
        const userId = res.locals.authInfo ? res.locals.authInfo.user.id : 0;
        Models.Todo.finishTodo( {
            id: req.params.id,
            isFinished: req.body.isFinished,
            userId: userId
        } ).then( todo => {
            res.json( successResponse( {
                todo: todo
            } ) );
        } ).catch( error => {
            res.json( errorResponse( "Cannot Update Todo.." ) );
        } );
    } );

    return router;
}
