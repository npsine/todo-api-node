import express from "express";
import UserApi from "./user.route";
import TodoApi from "./todo.route";

export default () => {
    const router = express.Router();

    router.use( "/user", UserApi() );
    router.use( "/todo", TodoApi() );

    return router;
};
