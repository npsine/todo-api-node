import path from "path";

export const BUILD_DIR = path.join(__dirname, "..", "..", "client", "build");
export const PUBLIC_DIR = path.join(__dirname, "..", "public");
export const LOG_DIR = path.join(__dirname, "..", "..", "logs");